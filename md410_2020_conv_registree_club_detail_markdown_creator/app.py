from collections import defaultdict
from datetime import datetime
import os.path

from md410_2020_conv_common.db import DB


class MarkdownCreator(object):
    def __init__(self, out_dir, fn="md410_convention_registrees_by_club.txt"):
        self.fn = os.path.abspath(os.path.normpath(os.path.join(out_dir, fn)))
        with open("clubs.txt", "r") as fh:
            self.clubs = [c.strip() for c in fh.readlines()]
        self.get_data()
        self.create_markdown()

    def get_data(self):
        self.md_clubs = defaultdict(list)
        self.non_md_clubs = defaultdict(list)
        self.partners = []
        dbh = DB()
        for registree in dbh.get_all_registrees():
            if not registree.is_lion:
                self.partners.append(registree)
            else:
                if registree.club not in self.clubs:
                    self.non_md_clubs[registree.club.replace("-", "Unknown")].append(registree)
                else:
                    self.md_clubs[registree.club].append(registree)

    def write_names(self, names_list):
        names = [f"* {r.last_name.strip()}, {r.first_names.strip()}" for r in names_list]
        names.sort()
        for n in names:
            self.fh.write(f"{n}\n")
        self.fh.write("\n")

    def create_markdown(self):
        with open(self.fn, "w") as self.fh:
            self.fh.write(
                f"<<heading:MD410 2020 Convention Registrees by Club:as at {datetime.now():%H.%M on %d %b %Y}>>\n\n"
            )
            total_attendees = 0
            for (t, a) in (("MD410 Clubs", self.md_clubs), ("Other Clubs", self.non_md_clubs)):
                num_attendees = sum([len(v) for v in a.values()])
                total_attendees += num_attendees
                self.fh.write(
                    f"# {t} - {num_attendees} attendee{'s' if num_attendees > 1 else ''}\n\n"
                )
                clubs = list(a.keys())
                clubs.sort()
                for c in clubs:
                    self.fh.write(
                        f"## {c} - {len(a[c])} attendee{'s' if len(a[c]) > 1 else ''}\n\n"
                    )
                    # print(c)
                    self.write_names(a[c])

            total_attendees += len(self.partners)
            self.fh.write(
                f"# Non-Lions - {len(self.partners)} attendee{'s' if len(self.partners) > 1 else ''}\n\n"
            )
            self.write_names(self.partners)

            self.fh.write(
                f"# Total - {total_attendees} attendee{'s' if total_attendees > 1 else ''}"
            )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Build MD410 2020 Convention registration record")
    parser.add_argument("--out_dir", default="/io/", help="The directory to write output to.")
    parser.add_argument("--fn", action="store_true", help="Output resulting filename")
    args = parser.parse_args()

    mc = MarkdownCreator(args.out_dir)
    if args.fn:
        print(mc.fn)
