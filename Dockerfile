FROM registry.gitlab.com/kimvanwyk/python3-poetry-container:latest

COPY md410_2020_conv_registree_club_detail_markdown_creator/*.py /app/
COPY md410_2020_conv_registree_club_detail_markdown_creator/clubs.txt /app/

ENTRYPOINT ["python", "/app/app.py", "--fn"]
